import { assertObjectMatch } from "@std/assert";
import { join } from "@std/path/join";
import { parseKcsConst } from "./cache_updater.ts";

Deno.test(async function parseKcsConstTest() {
  using kcsConst = await Deno.open(join("fixtures", "kcs_const.js"));

  assertObjectMatch(await parseKcsConst(kcsConst.readable), {
    versionInfo: "6.0.0.0",
    serverInfo: {
      World_1: "http://w01y.kancolle-server.com/",
      World_2: "http://203.104.209.87/",
      World_3: "http://125.6.184.215/",
      World_4: "http://203.104.209.183/",
      World_5: "http://203.104.209.150/",
      World_6: "http://203.104.209.134/",
      World_7: "http://203.104.209.167/",
      World_8: "http://203.104.209.199/",
      World_9: "http://125.6.189.7/",
      World_10: "http://125.6.189.39/",
      World_11: "http://125.6.189.71/",
      World_12: "http://125.6.189.103/",
      World_13: "http://125.6.189.135/",
      World_14: "http://125.6.189.167/",
      World_15: "http://125.6.189.215/",
      World_16: "http://125.6.189.247/",
      World_17: "http://203.104.209.23/",
      World_18: "http://203.104.209.39/",
      World_19: "http://203.104.209.55/",
      World_20: "http://203.104.209.102/",
    },
    maintenanceInfo: {
      IsDoing: 0,
      IsEmergency: 0,
      StartDateTime: 1733151600000,
      EndDateTime: 1733155199000,
    },
  });
});
