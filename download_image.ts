import { basename, join } from "@std/path";
import { emptyDir } from "@std/fs/empty-dir";
import { CACHE_DIR, CACHE_SERVERS } from "./cache_updater.ts";

const CACHE_ASSETS = join(CACHE_DIR, "assets");

export function resourceURL(origin: URL, filename: string): URL {
  return new URL(`/kcs2/resources/world/${filename}`, origin);
}
export async function outputEntries(): Promise<
  { origin: URL; path: string; image: { title: string; small: string } }[]
> {
  const serverInfo = await Deno.readTextFile(CACHE_SERVERS).then((f) =>
    JSON.parse(f) as Record<string, string>
  );

  return Object.entries(serverInfo).reduce(
    (prev, [name, originString]) => {
      const origin = new URL(originString);
      const filename = origin.hostname.split(".").map((x) =>
        `000${x}`.slice(-3)
      ).join("_");

      prev.push({
        origin,
        path: join(CACHE_ASSETS, name.toLowerCase()),
        image: {
          title: `${filename}_t.png`,
          small: `${filename}_s.png`,
        },
      });

      return prev;
    },
    [] as {
      origin: URL;
      path: string;
      image: { title: string; small: string };
    }[],
  );
}

function getBody(res: Response): ReadableStream<Uint8Array> {
  const { body } = res;
  if (body == null) {
    throw res.statusText;
  }

  return body;
}

async function download(
  origin: URL,
  path: string,
  imageName: string,
): Promise<[number, string]> {
  const savePath = join(path, imageName);
  const res = await fetch(resourceURL(origin, imageName)).then(getBody);

  using file = await Deno.open(savePath, {
    read: true,
    write: true,
    create: true,
    truncate: true,
  });
  await res.pipeTo(file.writable, { preventClose: true });
  const stat = await file.stat();
  file.close();

  return [stat.size, await Deno.realPath(savePath)];
}

if (import.meta.main) {
  const entries = await outputEntries();

  entries.map(async ({ origin, path, image }) => {
    const name = basename(path);
    await Deno.mkdir(path, { recursive: true });
    await emptyDir(path);

    console.log("download from %s (%s)", name, origin);
    return Promise.all([
      download(origin, path, image.small),
      download(origin, path, image.title),
    ]).then(([small, title]) => {
      console.log(`success: ${name}`);
      console.log(`path: ${small[1]}`, `size: ${small[0]}`);
      console.log(`path: ${title[1]}`, `size: ${title[0]}`);
    });
  });
}
